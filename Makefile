# Makefile for mysql-backup
#

install:
	cp -f mysql-backup /usr/local/sbin
	cp -f mysql-backup.cron /etc/cron.d/mysql-backup
	@echo "You need to setup /etc/mysql/backup.cnf manually"

uninstall:
	rm -f /usr/local/sbin/mysql-backup
	rm -f /etc/cron.d/mysql-backup
