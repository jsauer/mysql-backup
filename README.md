# Simple mysql backup script

This script will backup all mysql databases using mysqldump.
The backups will be compressed with `xz` and _not_ encrypted.

# Installation

1. Clone the repository by running: `git clone https://gitlab.com/jsauer/mysql-backup.git`
2. Run `make install` as superuser to install the script and cron file
3. Create mysql backup user:

```
CREATE USER 'backup'@'localhost' IDENTIFIED BY 'PASSWORD';
GRANT SELECT, LOCK TABLES ON *.* TO 'backup'@'localhost';
FLUSH PRIVILEGES;
```


4. Copy backup.cnf.example `cp backup.cnf.example /etc/mysql/backup.cnf` and adjust
   password.
5. Set ownership to `root` and permissions to `0600` to `/etc/mysql/backup.cnf`

# Default configuration
Default backup storage location: `/var/backup/mysql`  
Permission file for mysqldump: `/etc/mysql/backup.cnf`